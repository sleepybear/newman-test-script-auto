var newman = require('newman'),
    fs = require('fs'),
    path = require('path');

fs.readdir('./scripts', function (err, files) {
    if (err) { throw err; }

    // we filter all files with JSON file extension
    files = files.filter(file => {
        return (/^((?!(package(-lock)?))|.+)\.json/).test(file);
    });

    var testCases = files.reduce((accumulator, currentFile) => {
        var data = fs.readFileSync(`./scripts/${currentFile}`);
        if (accumulator.info === undefined) {
            return JSON.parse(data.toString());
        } else {
            var jsonData = JSON.parse(data.toString());
            accumulator.item = [...accumulator.item, ...jsonData.item];
            return accumulator;
        }
    }, {});

    // fs.writeFile("./tmp/test.json",  JSON.stringify(testCases), function(err) {
    //     if(err) {
    //         return console.log(err);
    //     }
    
    //     console.log("The file was saved!");
    // }); 

    newman.run({
        // we load collection using require. for better validation and handling
        // JSON.parse could be used
        collection: testCases,
        reporters: ['html', 'cli', 'json', 'junit']
    }, function (err, result) {
        // finally, when the collection executes, print the status
        console.info(err ? err : 'done!');
    });
});